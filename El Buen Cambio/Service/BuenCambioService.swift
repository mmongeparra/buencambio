//
//  BuenCambioService.swift
//  El Buen Cambio
//
//  Created by Manuel Monge on 10/15/21.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class BuenCambioService {

    //func getCurrency(completionHandler: @escaping (ApiResponse<[DataCurrency]>?) -> Void){
    func getCurrency(completionHandler: @escaping (ApiResponse?) -> Void){
        
        Alamofire.request(Constantes.urlCurrency, method: .get).responseObject {
            //(response: DataResponse<ApiResponse<[DataCurrency]>>) in
            (response: DataResponse<ApiResponse>) in
            
            switch response.result{
            case .success :
                if let response = response.result.value{
                    completionHandler(response)
                }else{
                    completionHandler(nil)
                }
            case .failure (let error):
                print(error)
                completionHandler(nil)
            }
        }
    }
}
