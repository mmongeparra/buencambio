//
//  CurrencyManager.swift
//  El Buen Cambio
//
//  Created by Manuel Monge on 10/16/21.
//

import Foundation
import UIKit

class CurrencyManager {
    
    class func getResult(digitado : Double, exchangeGive : DataCurrency, exchangeReceive : DataCurrency) -> Double {
        
        return (digitado * (exchangeGive.factorCompra)!) /                  (exchangeReceive.factorVenta)!
    }
    
    
    class func alertOperation(digitado : Double, exchangeGive : DataCurrency, exchangeReceive : DataCurrency) -> String {
        
        let base = "! Esta a punto de enviar {0} {1} para recibir {2} {3} ! \n ¿ Esta seguro de realizar la operación?"
        
        let cadenas = [UtilMethods.toString(valor: digitado),exchangeGive.name!,UtilMethods.toString(valor: self.getResult(digitado: digitado, exchangeGive: exchangeGive,  exchangeReceive: exchangeReceive)),exchangeReceive.name!]
        return base.formatWithCharacters(array: cadenas)
    }
    
    
    class func getConversion(exchangeGive : DataCurrency, exchangeReceive : DataCurrency) -> String {
        
        let base = "1 {0} {1} {2} {3} "
        let cadenas = [exchangeGive.symbol!,"=",UtilMethods.toString(valor: self.getResult(digitado: 1.00, exchangeGive: exchangeGive ,  exchangeReceive: exchangeReceive)),exchangeReceive.symbol!]
        return base.formatWithCharacters(array: cadenas)
    }
        
    class func getResume(exchangeGive : DataCurrency, exchangeReceive : DataCurrency) -> String {
        
        var cadena = ""
        let exchangeMayor : DataCurrency?
        let exchangeMenor : DataCurrency?
        let exchanges = UtilMethods.getCurrenciesLocal()
        let exchangeSoles = exchanges[0]
        var tipoexchange = 0
        
        if(exchangeGive.id == exchangeSoles.id || exchangeReceive.id == exchangeSoles.id){
            if (exchangeGive.factorCompra!) > (exchangeReceive.factorCompra!) {
                exchangeMayor = exchangeGive
                exchangeMenor = exchangeReceive
                tipoexchange = 1
            }else{
                exchangeMayor = exchangeReceive
                exchangeMenor = exchangeGive
                tipoexchange = 2
            }
        
            cadena = "{0} | {1}"
            var baseCompra = "Compra {0} {1}"
            var baseVenta = "Venta {0} {1}"
            
            let arrayCompra = [(exchangeMenor?.symbol!)!,UtilMethods.toString(valor: (exchangeMayor?.factorCompra)!)]
            baseCompra = UtilMethods.toBold(valor: baseCompra.formatWithCharacters(array : arrayCompra),
                                            bold : tipoexchange == 1)
            
            
            let arrayVenta = [(exchangeMenor?.symbol!)!,UtilMethods.toString(valor: (exchangeMayor?.factorVenta)!)]
            baseVenta = UtilMethods.toBold(valor: baseVenta.formatWithCharacters(array : arrayVenta),
                                            bold : tipoexchange == 2)
            
            cadena = cadena.formatWithCharacters(array: [baseCompra, baseVenta])
    
        }
        
        return cadena
    }
    
}
