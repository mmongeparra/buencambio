//
//  Constantes.swift
//  El Buen Cambio
//
//  Created by Manuel Monge on 10/15/21.
//

import Foundation
public class Constantes{
    
    static let service = "https://6169eba916e7120017fa0e6e.mockapi.io/"
    static let urlCurrency = URL(string: service + "currency/1")!
    
    static let keyCurrencies = "KEY_CURRENCIES"
    static let modalTitle = "El Buen Cambio"
    
}
