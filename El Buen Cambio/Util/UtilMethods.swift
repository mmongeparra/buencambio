//
//  UtilMethods.swift
//  El Buen Cambio
//
//  Created by Manuel Monge on 10/15/21.
//

import Foundation
import UIKit

class UtilMethods {
    
    class func toDouble(valor : String) -> Double? {
        
        return Double(valor)
    }
    
    class func toBold(valor : String, bold : Bool) -> String{
        if(bold){
            return "<b>" + valor + "</b>"
        }else{
            return valor
        }
    }
    class func toString(valor : Double) -> String {
        
        let value = String(format: "%.2f", valor)
        return value
    }
    class func validateCurrencies(currencies: [DataCurrency]?) -> Bool {
        var result = false
        if let curs = currencies{
            if curs.count > 2 {
                result = true
            }
        }
        return result
    }
    
    class func getHTMLAttribute (texto:String) -> NSAttributedString {
        
        let font = "System"
        
        let htmlString = "<html>" +
            "<head>" +
            "<style>" +
            "body {" +
            "font-family: '" +
            font +
            "';" +
            "font-size: 20px;" +
            "font-weight: normal;" +
            "color: black;" +
            "}" +
            "</style>" +
            "</head>" +
            "<body>" +
            texto +
        "</body></html>"
        
        return try! NSAttributedString(
            data: htmlString.data(using: String.Encoding.unicode, allowLossyConversion: false)!,options:[NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        
    }
    
    class func saveCurrenciesLocal(currencies: [DataCurrency]) {
        let data = try! NSKeyedArchiver.archivedData(withRootObject: currencies, requiringSecureCoding: false)
        UserDefaults().set(data, forKey: Constantes.keyCurrencies)
    }
    
    class func getCurrenciesLocal() ->  [DataCurrency] {
        var datalocal = [DataCurrency]()
        if let loadedData = UserDefaults().data(forKey: Constantes.keyCurrencies) {
            if let data =  try!  NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(loadedData)  as? [DataCurrency] {
                datalocal = data
            }
        }
        return datalocal
    }
    
    class func getCurrenciesOmitRemoved(omit : DataCurrency) ->[DataCurrency]{
        let list = getCurrenciesLocal()
        var newArray = [DataCurrency]()
        for (_, element) in list.enumerated() {
            if(element.id != omit.id){
                newArray.append(element)
            }
        }
        return newArray
    }
}


extension UIViewController {
    
    func alertMessage(message: String, completion: @escaping () -> Void) {
        let alertController = UIAlertController(title: Constantes.modalTitle, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction.init(title: "Aceptar", style: .default, handler: { (action) in
            completion()
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func alertMessage(message: String, options: String... , completion: @escaping (Int) -> Void) {
        let alertController = UIAlertController(title: Constantes.modalTitle, message: message, preferredStyle: .alert)
        for (index, option) in options.enumerated() {
            alertController.addAction(UIAlertAction.init(title: option, style: .default, handler: { (action) in
                completion(index)
            }))
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func toastMessage(message : String){
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        self.present(alert, animated: true)
            
        // duration in seconds
        let duration: Double = 2
            
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
            alert.dismiss(animated: true)
        }
    }
}

extension UITextField {
    func addDoneToolbar(onDone: (target: Any, action: Selector)? = nil) {
        let onDone = onDone ?? (target: self, action: #selector(doneButtonTapped))
        
        let toolbar: UIToolbar = UIToolbar()
        toolbar.barStyle = .black
        toolbar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: .done, target: onDone.target, action: onDone.action)
        ]
    
        
        toolbar.sizeToFit()
        self.inputAccessoryView = toolbar
    }
    
    // Default actions:
    @objc func doneButtonTapped() { self.resignFirstResponder() }
    
}

extension UIView {
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    func formatWithCharacters(array : [String]) -> String{
        var cadena = self
        for (index, element) in array.enumerated() {
            let reeemplazable = "{" + String(index) + "}"
            cadena = cadena.replacingOccurrences(of: reeemplazable, with: element)
        }
        return cadena
    }
}
