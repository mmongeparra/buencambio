//
//  CurrencyCell.swift
//  El Buen Cambio
//
//  Created by Manuel Monge on 10/16/21.
//
import UIKit
import Kingfisher
class CurrencyCell: UITableViewCell {
    
    @IBOutlet weak var imgCountry: UIImageView!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblConversion: UILabel!
    var data : DataCurrency?
    var omit : DataCurrency?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initView()
        // Initialization code
    }
    
    func initView(){
        if let d = data {
            lblCountry.text = d.country
            lblConversion.text = self.calculateConversion()
            if let imgCat = d.flag {
                if let encoded = imgCat.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed){
                    imgCountry.kf.setImage(with: ImageResource(downloadURL: URL(string: encoded)!) )
                }
            }
        }
    }
    
    func calculateConversion() -> String {
        return CurrencyManager.getConversion(exchangeGive: omit!, exchangeReceive: data!)
    }
}
