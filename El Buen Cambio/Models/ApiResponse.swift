//
//  CurrencyResponse.swift
//  El Buen Cambio
//
//  Created by Manuel Monge on 10/15/21.
//

import Foundation
import ObjectMapper

//class ApiResponse<Value>: Mappable {
class ApiResponse: Mappable {
    
    var status:Bool?
    var message:String?
    //var data: Value?
    var data: [DataCurrency]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        status <- map["status"]
        message <- map["message"]
        data <- map["data"]
    }
}


class DataCurrency: NSObject, NSCoding, Mappable {
    
    var id:Int?
    var symbol:String?
    var country:String?
    var name:String?
    var factorCompra:Double?
    var factorVenta:Double?
    var flag : String?
    
    init(id: Int, symbol: String, country: String,  name: String, factorCompra : Double, factorVenta : Double, flag : String) {
        self.id = id
        self.symbol = symbol
        self.country = country
        self.name  = name
        self.factorCompra  = factorCompra
        self.factorVenta  = factorVenta
        self.flag  = flag
    }
    
    required init?(map: Map) {
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as? Int
        self.symbol = aDecoder.decodeObject(forKey: "symbol") as? String
        self.country = aDecoder.decodeObject(forKey: "country") as? String
        self.name = aDecoder.decodeObject(forKey: "name") as? String
        self.factorCompra = aDecoder.decodeObject(forKey: "factorCompra") as? Double
        self.factorVenta = aDecoder.decodeObject(forKey: "factorVenta") as? Double
        self.flag = aDecoder.decodeObject(forKey: "flag") as? String
        
    }
    
    init(json: NSDictionary) {
        self.id = json["id"] as? Int
        self.symbol =  json["symbol"] as? String
        self.country =  json["country"] as? String
        self.name =  json["name"] as? String
        self.factorCompra =  json["factorCompra"] as? Double
        self.factorVenta =  json["factorVenta"] as? Double
        self.flag =  json["flag"] as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.symbol, forKey: "symbol")
        aCoder.encode(self.country, forKey: "country")
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.factorCompra, forKey: "factorCompra")
        aCoder.encode(self.factorVenta, forKey: "factorVenta")
        aCoder.encode(self.flag, forKey: "flag")
        
    }
    func mapping(map: Map) {
        id <- map["id"]
        symbol <- map["symbol"]
        country <- map["country"]
        name <- map["name"]
        factorCompra <- map["factorCompra"]
        factorVenta <- map["factorVenta"]
        flag <- map["flag"]
    }
}

