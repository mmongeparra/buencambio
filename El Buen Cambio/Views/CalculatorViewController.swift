//
//  CalculatorViewController.swift
//  El Buen Cambio
//
//  Created by Manuel Monge on 10/15/21.
//

import UIKit

class CalculatorViewController: UIViewController, CalculatorViewDelegate {
   
    @IBOutlet weak var lblCalculado: UILabel!
    
    @IBOutlet weak var lblTipoCambio: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var editCalculator: UITextField!
    @IBOutlet weak var btnReceive: UIButton!
    @IBOutlet weak var btnGive: UIButton!
    private let calculatorPresenter = CalculatorPresenter()
    var statusCalculator = false
    
    var omit : DataCurrency?
    var selectGive = false
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
        calculatorPresenter.setViewDelegate(calculatorViewDelegate: self)
        calculatorPresenter.loadInitData()
    }
    
    func initView(){
        self.btnReceive.roundCorners([.bottomRight], radius: 20)
        self.btnGive.roundCorners([.topRight], radius: 20)
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.long_press_give(_:)))
        self.btnGive.addGestureRecognizer(longPress)
        let longPressReceive = UILongPressGestureRecognizer(target: self, action: #selector(self.long_press_receive(_:)))
        self.btnReceive.addGestureRecognizer(longPressReceive)
        editCalculator.text = "100"
        editCalculator.addDoneToolbar(onDone: (target: self, action: #selector(self.submitResult)))
    }
    
    @objc func long_press_give(_ gesture: UILongPressGestureRecognizer) {
        if gesture.state == UIGestureRecognizer.State.began {
            omit = calculatorPresenter.getReceive()
            selectGive = true
            self.performSegue(withIdentifier: "segue_currencies", sender: self)
        }
    }
    
    @objc func long_press_receive(_ gesture: UILongPressGestureRecognizer) {
        if gesture.state == UIGestureRecognizer.State.began {
            omit = calculatorPresenter.getGive()
            selectGive = false
            self.performSegue(withIdentifier: "segue_currencies", sender: self)
        }
    }
    
    
    @objc func submitResult(_ sender: Any) {
        calculatorPresenter.calculateResult(data : editCalculator.text!)
        editCalculator.resignFirstResponder()
    }
    
    func updateExchangeType(nameOrigin :(String),nameDestiny : (String), resume : (String)){
        btnGive.setTitle(nameOrigin, for: .normal)
        btnReceive.setTitle(nameDestiny, for: .normal)
        lblTipoCambio.attributedText = UtilMethods.getHTMLAttribute(texto: resume)
        lblTipoCambio.textAlignment = .center
        calculatorPresenter.calculateResult(data : editCalculator.text!)
    
    }
    
    func updateResult(result :(String),status : (Bool)){
        statusCalculator = status
        if(statusCalculator){
            lblCalculado.text = result
        }else{
            lblCalculado.text = "Texto erroneo"
        }
    }
    
    @IBAction func press_swap(_ sender: Any) {
        calculatorPresenter.swapExchanges()
    }
    
    func updateExchange(currency: (DataCurrency)) {
        calculatorPresenter.updateExchange(currency : currency,selectGive : selectGive)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segue_currencies" {
            if let destinationVC = segue.destination as? SelectorViewController
            {
                destinationVC.omit = self.omit
                destinationVC.selectGive = self.selectGive
                destinationVC.calculatorViewDelegate = self
            }
        }
    }
    @IBAction func operacion_pressed(_ sender: Any) {
        calculatorPresenter.operationSubmit(data : editCalculator.text!)
    }
    
    func operationAlert(texto: (String)) {
        self.alertMessage(message: texto, options : "OK","Cancelar", completion : {
            (operation)
            in
            switch(operation){
                case 0:
                    self.toastMessage(message : "Operacion exitosa")
                    break
                default:
                    print("no hace nada")
                    break
            }
            
        })
    }
    
    func alertError(texto: (String)) {
        self.alertMessage(message: texto, completion : {
            
        })
    }
}
