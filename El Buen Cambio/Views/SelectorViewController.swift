//
//  SelectorViewController.swift
//  El Buen Cambio
//
//  Created by Manuel Monge on 10/16/21.
//

import UIKit

class SelectorViewController: UIViewController, SelectorViewDelegate {
    
    @IBOutlet weak var tblCurrencies: UITableView!
    private let selectorPresenter = SelectorPresenter()
    var calculatorViewDelegate : CalculatorViewDelegate?
    var items = [DataCurrency]()
    var omit : DataCurrency?
    var itemSelected : DataCurrency?
    var selectGive = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
        selectorPresenter.setViewDelegate(selectorViewDelegate: self, calculatorViewDelegate : calculatorViewDelegate)
        selectorPresenter.loadCurrencies(omit : omit!)
    }
    
    func initView(){
        self.tblCurrencies.delegate = self
        self.tblCurrencies.dataSource = self
        let cellNib = UINib(nibName: "CurrencyCell", bundle: nil)
        self.tblCurrencies.register(cellNib, forCellReuseIdentifier: "currencyCell")
        self.tblCurrencies.showsVerticalScrollIndicator = true
    }
    
    
    func reloadTableCurrencies(currencies:([DataCurrency])) {
        self.items = currencies
        self.tblCurrencies.reloadData()
    }
}

extension SelectorViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        itemSelected = self.items[indexPath.row]
        self.selectorPresenter.updateExchange(currency : itemSelected!)
        self.dismiss(animated: true, completion: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblCurrencies.dequeueReusableCell(withIdentifier: "currencyCell") as! CurrencyCell
        let dataItem = self.items[indexPath.row]
        cell.data = dataItem
        cell.omit = self.omit
        cell.initView()
        return cell
    }
}


