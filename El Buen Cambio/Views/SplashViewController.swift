//
//  ViewController.swift
//  El Buen Cambio
//
//  Created by Manuel Monge on 10/15/21.
//

import UIKit

class SplashViewController: UIViewController, SplashViewDelegate {
    
    private let splashPresenter = SplashPresenter(buenCambioService: BuenCambioService())
        
    override func viewDidLoad() {
        super.viewDidLoad()
        splashPresenter.setViewDelegate(splashViewDelegate: self)
        splashPresenter.loadCurrencies()
    }
    
    func showResultCurrencies(status:(Bool)) {
        
        if(status){
            self.performSegue(withIdentifier: "segue_calculator", sender: self)
        }else{
            self.alertMessage(message: "Ocurrió un error en la consulta", completion : {
                
            })
        }
        //descriptionLabel.text = description
    }
}




