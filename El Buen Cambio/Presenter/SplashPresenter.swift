//
//  SplashPresenter.swift
//  El Buen Cambio
//
//  Created by Manuel Monge on 10/15/21.
//

import Foundation
import UIKit

protocol SplashViewDelegate: NSObjectProtocol {
    func showResultCurrencies(status:(Bool))
}

class SplashPresenter {
    private let buenCambioService: BuenCambioService
    weak private var splashViewDelegate : SplashViewDelegate?
    
    init(buenCambioService:BuenCambioService){
        self.buenCambioService = buenCambioService
    }
    
    func setViewDelegate(splashViewDelegate:SplashViewDelegate?){
        self.splashViewDelegate = splashViewDelegate
    }
    
    func loadCurrencies(){
        buenCambioService.getCurrency(){
            (respuesta) in
            var status = false
            if let respuesta = respuesta{
                if(UtilMethods.validateCurrencies(currencies: respuesta.data)){
                    status = true
                    UtilMethods.saveCurrenciesLocal(currencies: respuesta.data!)
                }
            }
            self.splashViewDelegate!.showResultCurrencies(status: status)
        }
    }
}
