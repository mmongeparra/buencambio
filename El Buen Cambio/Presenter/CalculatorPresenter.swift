//
//  CalculatorPresenter.swift
//  El Buen Cambio
//
//  Created by Manuel Monge on 10/15/21.
//

import Foundation
import UIKit

protocol CalculatorViewDelegate: NSObjectProtocol {
    //func showResultCurrencies(status:(Bool))
    func operationAlert(texto :(String))
    func alertError(texto :(String))
    func updateExchange(currency :(DataCurrency))
    func updateResult(result :(String),status : (Bool))
    func updateExchangeType(nameOrigin :(String),nameDestiny : (String), resume : (String))
}

class CalculatorPresenter {
    weak private var calculatorViewDelegate : CalculatorViewDelegate?
    var exchangeGive : DataCurrency?
    var exchangeReceive : DataCurrency?
    init(){
        
    }
    
    func setViewDelegate(calculatorViewDelegate:CalculatorViewDelegate?){
        self.calculatorViewDelegate = calculatorViewDelegate
    }
    
    func loadInitData(){
        let exchanges = UtilMethods.getCurrenciesLocal()
        exchangeGive = exchanges[0]
        exchangeReceive = exchanges[1]
        self.changeExchange()
    }
    
    func swapExchanges(){
        let tempOrigin = exchangeGive
        let tempDestiny = exchangeReceive
        exchangeGive = tempDestiny
        exchangeReceive = tempOrigin
        self.changeExchange()
    }
    
    func updateExchange(currency :(DataCurrency), selectGive : (Bool)){
        if(selectGive){
            exchangeGive = currency
        }else{
            exchangeReceive = currency
        }
        self.changeExchange()
    }
    
    func changeExchange(){
        let originName = exchangeGive?.name!
        let destinyName = exchangeReceive?.name!
        let resume = getResume()
        self.calculatorViewDelegate?.updateExchangeType(nameOrigin: originName!, nameDestiny: destinyName!, resume : resume)
    }
    
    func calculateResult(data : String){
        let number = UtilMethods.toDouble(valor: data)
        if let digitado = number {
            let result = CurrencyManager.getResult(digitado: digitado, exchangeGive: exchangeGive!, exchangeReceive: exchangeReceive!)
            self.calculatorViewDelegate?.updateResult(result: UtilMethods.toString(valor: result), status: true)
        }else{
            self.calculatorViewDelegate?.updateResult(result: "", status: false)
        }
    }
    
    func operationSubmit(data : String){
        let number = UtilMethods.toDouble(valor: data)
        if let digitado = number {
            let result = CurrencyManager.alertOperation(digitado: digitado, exchangeGive: exchangeGive!, exchangeReceive: exchangeReceive!)
            self.calculatorViewDelegate?.operationAlert(texto : result)
        }else{
            self.calculatorViewDelegate?.alertError(texto : "No se pudo enviar")
        }
    }
    
    func getResume() -> String {
        return CurrencyManager.getResume(exchangeGive: exchangeGive!,
                                         exchangeReceive: exchangeReceive!)
    }
    
    func getGive() -> DataCurrency {
        return exchangeGive!
    }
    
    func getReceive() -> DataCurrency {
        return exchangeReceive!
    }
    
    
}

