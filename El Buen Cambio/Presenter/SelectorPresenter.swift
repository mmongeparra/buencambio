//
//  SelectorPresenter.swift
//  El Buen Cambio
//
//  Created by Manuel Monge on 10/16/21.
//

import Foundation
import UIKit

protocol SelectorViewDelegate: NSObjectProtocol {
    func reloadTableCurrencies(currencies:([DataCurrency]))
}

class SelectorPresenter {
    weak private var selectorViewDelegate : SelectorViewDelegate?
    weak private var calculatorViewDelegate : CalculatorViewDelegate?
    init(){
    }
    
    func setViewDelegate(selectorViewDelegate:SelectorViewDelegate?,calculatorViewDelegate : CalculatorViewDelegate?){
        self.selectorViewDelegate = selectorViewDelegate
        self.calculatorViewDelegate = calculatorViewDelegate
    }
    
    func loadCurrencies(omit : DataCurrency){
        let list = UtilMethods.getCurrenciesOmitRemoved(omit: omit)
        selectorViewDelegate?.reloadTableCurrencies(currencies: list)
    }
    
    func updateExchange(currency :(DataCurrency)){
        calculatorViewDelegate?.updateExchange(currency : currency)
    }
}
