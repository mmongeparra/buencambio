//
//  ViewRoundBorder.swift
//  El Buen Cambio
//
//  Created by Manuel Monge on 10/15/21.
//

import Foundation
import UIKit

class ViewRoundBorder: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 20
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.masksToBounds = false
    }
}
